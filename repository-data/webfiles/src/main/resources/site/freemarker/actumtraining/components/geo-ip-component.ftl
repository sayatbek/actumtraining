<#include "../../include/imports.ftl">
<#import "../components/templates/geo-response-card.ftl" as singleResponse>
<div align="center">
  Geo Ip Servie Component
</div>

<div>
  <h3>Default Ip address set by content manager is: </h3> ${defaultIpAddress} <br>
</div>

<br>

<div class="row">
  <div class="col-md-6">
    <h4>Information provided from GeoIp service is:</h4><br>
    <@singleResponse.geoResponseCard geoResponce=geoResponce />
  </div>
  <#if pageable??>
  <div class="col-md-6">
    <h4>Information taken from selected document is:</h4><br>
    <#list pageable.items as item>
      <@singleResponse.geoResponseCard geoResponce=item />
      <br>
    </#list>
  </div>
  </#if>
</div>

<div>
  <span>Enter the Ip address you want to request (update here): </span>
  <form id="form-ip" action="" method="post" enctype='application/json'>
    <input type="text" name="ip-address" value="83.110.250.231"/>
    <input type="submit" value="ok">
  </form>
</div>

<br>

<div>
  <span>Enter the Ip address you want to request (different page): </span>
  <form id="form-ip" action="result-page" method="post" enctype='application/json'>
    <input type="text" name="ip-address" value="188.26.232.61"/>
    <input type="submit" value="ok">
  </form>
</div>

<br>

<div>
  <span>REST endpoint: </span>
  <a href="http://localhost:8080/site/restservices/geoip/get?input=218.107.132.66" target="_blank">
    http://localhost:8080/site/restservices/geoip/get?input=218.107.132.66
  </a>
</div>
