<#include "../../../include/imports.ftl">
<#macro geoResponseCard geoResponce>
  <span>City: </span>${geoResponce.city} <br>
  <span>Country: </span>
    <#if geoResponce.country?is_hash>${geoResponce.country.name}<#else>${geoResponce.country}</#if> <br>
  <span>Latitude: </span>
    <#if geoResponce.location??>${geoResponce.location.latitude}<#else>${geoResponce.latitude}</#if> <br>
  <span>Longitude: </span>
    <#if geoResponce.location??>${geoResponce.location.longitude}<#else>${geoResponce.longitude}</#if> <br>
  <span>Time Zone: </span>
    <#if geoResponce.location??>${geoResponce.location.timeZone}<#else>${geoResponce.timezone}</#if> <br>
</#macro>