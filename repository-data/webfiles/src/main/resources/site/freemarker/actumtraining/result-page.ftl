<#include "../include/imports.ftl">
<#import "components/templates/geo-response-card.ftl" as singleResponse>

<div>
  <span>Information provided from GeoIp service is:</span><br>
  <@singleResponse.geoResponseCard geoResponce=geoResponce />
</div>