package com.actum.training.controllers.rest;

import com.actum.training.models.GeoResponse;
import com.actum.training.services.GeoIpService;
import com.google.gson.Gson;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import org.hippoecm.hst.jaxrs.services.AbstractResource;
import org.hippoecm.hst.site.HstServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Path("/geoip")
public class GeoIpRestController extends AbstractResource {

  static final Logger logger = LoggerFactory.getLogger(GeoIpRestController.class);

  @GET
  @Path("/get")
  @Produces("application/json")
  public String getDataByIp(
      @Context HttpServletRequest servletRequest,
      @Context HttpServletResponse servletResponse,
      @Context UriInfo uriInfo,
      @FormParam("input") String ipAdress) {

    logger.debug(ipAdress);
    GeoIpService geoIpService = HstServices.getComponentManager()
        .getComponent("geoIpService");
    GeoResponse geoResponse = geoIpService.getDataByIp(ipAdress);

    String data = (new Gson()).toJson(geoResponse);
    return data;
  }
}
