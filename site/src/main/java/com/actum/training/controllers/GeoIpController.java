package com.actum.training.controllers;

import com.actum.training.infos.GeoIpInfo;
import com.actum.training.models.GeoResponse;
import com.actum.training.services.GeoIpService;
import java.util.ArrayList;
import java.util.List;
import org.hippoecm.hst.content.beans.standard.HippoDocument;
import org.hippoecm.hst.core.component.HstComponentException;
import org.hippoecm.hst.core.component.HstRequest;
import org.hippoecm.hst.core.component.HstResponse;
import org.hippoecm.hst.core.parameters.ParametersInfo;
import org.hippoecm.hst.site.HstServices;
import org.onehippo.cms7.essentials.components.CommonComponent;
import org.onehippo.cms7.essentials.components.EssentialsListPickerComponent;
import org.onehippo.cms7.essentials.components.paging.Pageable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ParametersInfo(
    type = GeoIpInfo.class
)
public class GeoIpController extends CommonComponent {

  static final Logger logger = LoggerFactory.getLogger(GeoIpController.class);
  GeoIpService geoIpService = HstServices.getComponentManager()
      .getComponent("geoIpService");

  public void doBeforeRender(HstRequest request, HstResponse response)
      throws HstComponentException {
    super.doBeforeRender(request, response);
    GeoIpInfo paramInfo = this.getComponentParametersInfo(request);
    List<HippoDocument> documentItems = this.getDocumentItems(paramInfo);

    int pageSize = documentItems.size();
    int page = this.getAnyIntParameter(request, "page", 1);
    Pageable<HippoDocument> pageable = this.getPageableFactory()
        .createPageable(documentItems, page, pageSize);
    request.setAttribute("pageable", pageable);
    request.setAttribute("cparam", paramInfo);

    String ipAddress = this.getPublicRequestParameter(request, "ip-address");
    if (ipAddress == null) {
      ipAddress = paramInfo.getDefaultIp();
    }

    GeoResponse geoResponse = geoIpService.getDataByIp(ipAddress);

    request.setAttribute("defaultIpAddress", paramInfo.getDefaultIp());
    request.setAttribute("geoResponce", geoResponse);
  }

  public List<HippoDocument> getDocumentItems(GeoIpInfo componentInfo) {
    List<HippoDocument> beans = new ArrayList<>();
    this.addBeanForPath(componentInfo.getDocumentItem1(), beans);
    this.addBeanForPath(componentInfo.getDocumentItem2(), beans);
    return beans;
  }
}
