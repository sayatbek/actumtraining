package com.actum.training.infos;

import org.hippoecm.hst.core.parameters.FieldGroup;
import org.hippoecm.hst.core.parameters.FieldGroupList;
import org.hippoecm.hst.core.parameters.JcrPath;
import org.hippoecm.hst.core.parameters.Parameter;

@FieldGroupList({
    @FieldGroup(titleKey = "IP", value = {"defaultIp"}),
    @FieldGroup(titleKey = "Picker", value = {"document1", "document2"})
})
public interface GeoIpInfo {

  @Parameter(
      name = "defaultIp",
      displayName = "Default Ip Address",
      defaultValue = "109.40.1.137"
  )
  String getDefaultIp();

  @Parameter(name = "document1",
      displayName = "Document1",
      required = false)
  @JcrPath(
      isRelative = true,
      pickerConfiguration = "cms-pickers/documents",
      pickerSelectableNodeTypes = "actumtraining:GeoLocation")
  String getDocumentItem1();

  @Parameter(name = "document2",
      displayName = "Document2",
      required = false)
  @JcrPath(
      isRelative = true,
      pickerConfiguration = "cms-pickers/documents",
      pickerSelectableNodeTypes = "actumtraining:GeoLocation")
  String getDocumentItem2();
}
