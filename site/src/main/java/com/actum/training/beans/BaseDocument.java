package com.actum.training.beans;

import org.hippoecm.hst.content.beans.Node;
import org.hippoecm.hst.content.beans.standard.HippoDocument;

@Node(jcrType = "actumtraining:basedocument")
public class BaseDocument extends HippoDocument {

}
