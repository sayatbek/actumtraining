package com.actum.training.beans;

import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;
import org.hippoecm.hst.content.beans.Node;

@HippoEssentialsGenerated(internalName = "actumtraining:GeoLocation")
@Node(jcrType = "actumtraining:GeoLocation")
public class GeoLocation extends BaseDocument {
    @HippoEssentialsGenerated(internalName = "actumtraining:city")
    public String getCity() {
        return getProperty("actumtraining:city");
    }

    @HippoEssentialsGenerated(internalName = "actumtraining:country")
    public String getCountry() {
        return getProperty("actumtraining:country");
    }

    @HippoEssentialsGenerated(internalName = "actumtraining:latitude")
    public Double getLatitude() {
        return getProperty("actumtraining:latitude");
    }

    @HippoEssentialsGenerated(internalName = "actumtraining:longitude")
    public Double getLongitude() {
        return getProperty("actumtraining:longitude");
    }

    @HippoEssentialsGenerated(internalName = "actumtraining:timezone")
    public String getTimezone() {
        return getProperty("actumtraining:timezone");
    }
}
