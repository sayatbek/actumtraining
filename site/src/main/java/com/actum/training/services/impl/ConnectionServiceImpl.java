package com.actum.training.services.impl;

import com.actum.training.services.ConnectionService;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class ConnectionServiceImpl implements ConnectionService {

  static final Logger logger = LoggerFactory.getLogger(ConnectionServiceImpl.class);

  @Override
  public String makeConnection(String link, String connectionType) {
    logger.debug(link);
    String result = "";
    try {
      URL url = new URL(link);
      HttpURLConnection conn = (HttpURLConnection) url.openConnection();
      conn.setRequestMethod(connectionType);
      conn.setRequestProperty("Accept", "application/json; charset=utf-8");

      if (conn.getResponseCode() != 200) {
        throw new RuntimeException("Failed : HTTP error code : "
            + conn.getResponseCode());
      }

      BufferedReader br = new BufferedReader(
          new InputStreamReader(conn.getInputStream(), StandardCharsets.UTF_8));

      String output;
      while ((output = br.readLine()) != null) {
        result += output;
      }

      conn.disconnect();
    } catch (MalformedURLException e) {
      logger.info(e.getLocalizedMessage());
    } catch (IOException e) {
      logger.info(e.getLocalizedMessage());
    }
    return result;
  }
}
