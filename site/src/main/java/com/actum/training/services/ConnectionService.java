package com.actum.training.services;

public interface ConnectionService {

  String makeConnection(String link, String connectionType);
}
