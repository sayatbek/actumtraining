package com.actum.training.services;

import com.actum.training.models.GeoResponse;
import javax.servlet.http.HttpServletRequest;

public interface GeoIpService {

  GeoResponse getDataByIp(String ipAdress);

  String getClientIpAddress(HttpServletRequest request);
}
