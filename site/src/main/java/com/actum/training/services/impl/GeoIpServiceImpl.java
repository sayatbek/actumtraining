package com.actum.training.services.impl;

import com.actum.training.models.GeoResponse;
import com.actum.training.services.ConnectionService;
import com.actum.training.services.GeoIpService;
import com.google.gson.Gson;
import javax.servlet.http.HttpServletRequest;
import lombok.Getter;
import lombok.Setter;
import org.hippoecm.hst.site.HstServices;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Setter
@Getter
@Service
public class GeoIpServiceImpl implements GeoIpService {

  String geoIpServiceUrl;

  static final Logger logger = LoggerFactory.getLogger(GeoIpServiceImpl.class);

  @Override
  public GeoResponse getDataByIp(String ipAddress) {
    String url = geoIpServiceUrl + ipAddress;

    GeoResponse geoResponse = null;
    ConnectionService connectionService = HstServices.getComponentManager()
        .getComponent("connectionService");

    try {
      String jsonResponse = connectionService.makeConnection(url, "GET");
      geoResponse = new Gson().fromJson(jsonResponse, GeoResponse.class);
    } catch (RuntimeException e) {
      logger.info(e.getLocalizedMessage());
    }

    return geoResponse;
  }

  @Override
  public String getClientIpAddress(HttpServletRequest request) {
    String ip = request.getHeader("X-Forwarded-For");
    if (ip == null || ip.length() == 0 || ip.equalsIgnoreCase("unknown")) {
      ip = request.getHeader("Proxy-Client-IP");
    }
    if (ip == null || ip.length() == 0 || ip.equalsIgnoreCase("unknown")) {
      ip = request.getHeader("WL-Proxy-Client-IP");
    }
    if (ip == null || ip.length() == 0 || ip.equalsIgnoreCase("unknown")) {
      ip = request.getHeader("HTTP_X_FORWARDED_FOR");
    }
    if (ip == null || ip.length() == 0 || ip.equalsIgnoreCase("unknown")) {
      ip = request.getHeader("HTTP_X_FORWARDED");
    }
    if (ip == null || ip.length() == 0 || ip.equalsIgnoreCase("unknown")) {
      ip = request.getHeader("HTTP_X_CLUSTER_CLIENT_IP");
    }
    if (ip == null || ip.length() == 0 || ip.equalsIgnoreCase("unknown")) {
      ip = request.getHeader("HTTP_CLIENT_IP");
    }
    if (ip == null || ip.length() == 0 || ip.equalsIgnoreCase("unknown")) {
      ip = request.getHeader("HTTP_FORWARDED_FOR");
    }
    if (ip == null || ip.length() == 0 || ip.equalsIgnoreCase("unknown")) {
      ip = request.getHeader("HTTP_FORWARDED");
    }
    if (ip == null || ip.length() == 0 || ip.equalsIgnoreCase("unknown")) {
      ip = request.getHeader("HTTP_VIA");
    }
    if (ip == null || ip.length() == 0 || ip.equalsIgnoreCase("unknown")) {
      ip = request.getHeader("REMOTE_ADDR");
    }
    if (ip == null || ip.length() == 0 || ip.equalsIgnoreCase("unknown")) {
      ip = request.getRemoteAddr();
    }
    return ip;
  }

}
