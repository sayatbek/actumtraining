package com.actum.training.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
public class GeoResponse {

  String city;
  Country country;
  Location location;
  String ip;
}