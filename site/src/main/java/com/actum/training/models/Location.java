package com.actum.training.models;

import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
public class Location {

  @SerializedName("accuracy_radius")
  Integer accuracyRadius;
  Double latitude;
  Double longitude;

  @SerializedName("time_zone")
  String timeZone;
}
